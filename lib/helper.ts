export default class Helper {
  private name: string;
  constructor(name: string) {
    console.log("ctor", name);
  }
  public getName(): string {
    return this.name.toUpperCase();
  }
}
