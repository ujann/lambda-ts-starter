import "source-map-support/register";
import { Context, Callback } from "aws-lambda";
import Helper from "./lib/helper";

export const handler = async (
  event: unknown,
  context: Context,
  callback: Callback
): Promise<void> => {
  console.log(
    "Lambda triggered",
    event,
    context,
    new Helper("TheName").getName()
  );
  callback(null, {});
};
